#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QtNetwork/QNetworkAccessManager>
#include "qwkpacket.h"

#define VERSION "1.2.4"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void doupdate();
    void load();
    QwkPacket *qwkpacket;
    bool intzip;
    QString archive_cmd;
    QString unarchive_cmd;
    QString signature;
    QByteArray msgViewState;
    QByteArray grpViewState;
    QString editor_font;
    qreal editor_font_size;
    bool qwkesupport;
    QString tagline_file;
    bool hide_empty_areas;
    QNetworkAccessManager *qnm;
    QList<QMenu *> bulletinMenuItems;
    void ftpDownloadFinished(bool success, QString reason = "An FTP Error Occured");
    void ftpUploadFinished(bool success, QString reason = "An FTP Error Occured");
private slots:
    void on_actionOpen_QWK_Packet_triggered();

    void on_msgTable_cellClicked(int row, int column);

    void on_replyButton_clicked();

    void on_newMsgButton_clicked();

    void on_actionSave_REP_Packet_triggered();

    void on_actionQuit_triggered();

    void on_actionAbout_triggered();

    void on_actionSettings_triggered();

    void on_MainWindow_destroyed();

    void on_areaList_cellClicked(int row, int column);

    void on_msgTable_itemSelectionChanged();

    void on_areaList_itemSelectionChanged();

    void on_actionHide_Empty_Areas_toggled(bool arg1);

    void on_actionFetch_QWK_via_FTP_triggered();

    void on_actionPut_REP_via_FTP_triggered();

    void on_actionAbout_QT_triggered();

    void on_actionView_Outbound_triggered();

private:
    void showBulletin(QString b);
    void openQwkFile(QString fileName);
    bool createRepPacket(QString fileName);
    Ui::MainWindow *ui;
    int selected_group;
    int selected_msg;
    void refresh_area_list();
    QByteArray tempdata;
};
#endif // MAINWINDOW_H
