#include "mainwindow.h"
#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    ui->label_2->setText("Titan Mail v" + QString(VERSION));
}

About::~About()
{
    delete ui;
}

void About::on_buttonBox_accepted()
{

}

void About::on_buttonBox_clicked(QAbstractButton *button)
{
      this->close();
}
