#ifndef OUTBOUND_H
#define OUTBOUND_H

#include <vector>
#include <QDialog>
#include "qwkheader.h"

class MainWindow;

struct ob_msg {
    QString filename;
    QString area;
    QString from;
    QString to;
    QString subj;
    QString body;
    struct QwkHeader hdr;
};

namespace Ui {
class Outbound;
}

class Outbound : public QDialog
{
    Q_OBJECT

public:
    explicit Outbound(QWidget *parent = nullptr);
    ~Outbound();
    bool load();

private slots:
    void on_deleteButton_clicked();

    void on_editButton_clicked();

private:
    bool read_message(QString filename, struct ob_msg *obmsg);
    Ui::Outbound *ui;
    MainWindow *mw;
    std::vector<struct ob_msg> messages;
};

#endif // OUTBOUND_H
