#include "bulletin.h"
#include "ui_bulletin.h"

#include <QDir>
#include <QStandardPaths>
#include <sstream>

Bulletin::Bulletin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Bulletin)
{
    ui->setupUi(this);
}

extern std::vector<std::string> demangle_ansi(const char *msg, size_t len);

bool Bulletin::load(QString file) {
    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    FILE *fptr;
    QDir dir(pth + "/_work");

    if (!dir.exists()) {
        return false;
    }

    fptr = fopen(dir.filePath(file).toStdString().c_str(), "r");

    if (!fptr) {
        return false;
    }

    std::stringstream ss;

    char c = fgetc(fptr);
    while (!feof(fptr) && c != 0x1a) {
        ss << c;
        c = fgetc(fptr);
    }

    ss << "\r\n";

    fclose(fptr);

    std::vector<std::string> bul = demangle_ansi(ss.str().c_str(), ss.str().size());

    QString ansibody = "";
    ansibody = "<div style=\"white-space:pre;\">";
    for (size_t p = 0; p < bul.size(); p++) {
        ansibody = ansibody + QString::fromStdString(bul.at(p));
    }
    ansibody = ansibody + "</div>";

    ui->bulText->setHtml(ansibody);

    setWindowTitle(file);

    return true;
}

Bulletin::~Bulletin()
{
    delete ui;
}
