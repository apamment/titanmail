#ifndef QWKPACKET_H
#define QWKPACKET_H

#include <QString>
#include <QList>
#include "messagegroup.h"
class QwkPacket
{
public:
    QwkPacket();
    ~QwkPacket();
    bool unpack_zip(QString filename);
    bool unpack(QString filename, QString unarchive_cmd);
    bool load_qwk(bool qwkesupport);
    void update_read(uint32_t offset, bool read);

    QList<messagegroup *> *groups;
    QList<QString> bulletins;
    QString bbsname;
    QString bbslocation;
    QString sysopname;
    QString username;
    QString bbsqwkid;
    QString packetdate;
    QString getLastError();
    static std::string convert_cp437(std::string input);
    static std::string convert_utf8(std::string input);
private:


    QString lastError;
};

#endif // QWKPACKET_H
