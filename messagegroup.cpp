#include "messagegroup.h"

messagegroup::messagegroup(int no)
{
    groupno = no;
    groupname = "";
    msgs = new QList<Message *>();
    personal = 0;
    unread = 0;
}

messagegroup::~messagegroup()
{
    delete msgs;
}
