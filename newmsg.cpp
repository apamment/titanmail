#include <QStandardPaths>
#include <QTemporaryFile>
#include <QDate>
#include <QDir>
#include <fstream>
#include "newmsg.h"
#include "ui_NewMsg.h"
#include "qwkheader.h"
#include "mainwindow.h"
#include "taglineselect.h"

NewMsg::NewMsg(QWidget *parent, QString subject, QString to, QString quote, QString bbstag, int area, QString username, int replyid, bool qwke) :
    QDialog(parent),
    ui(new Ui::NewMsg)
{
    ui->setupUi(this);
    ui->subjEdit->setText(subject);
    ui->toEdit->setText(to);
    ui->msgEdit->setPlainText(quote);
    qwkid = bbstag;
    qwkesupport = qwke;
    areano = area;
    this->username = username;
    this->replyid = replyid;
    this->mw = (MainWindow *)parent;
}

NewMsg::~NewMsg()
{
    delete ui;
}

void NewMsg::addMessage(QString msgtag) {
    QString SavePath = QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkid + "/" + "msg.XXXXXX");
    QDir p;
    p.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkid + "/");
    QTemporaryFile file(SavePath);
    struct QwkHeader qhdr;
    char buffer[64];
    if (file.open()) {
        file.setAutoRemove(false);
        QString filename = file.fileName(); // stop removing my file!!
        if (ui->pvtCheck->isChecked()) {
            qhdr.Msgstat = '+';
        } else {
            qhdr.Msgstat = ' ';
        }

        QString qwkestr = "";

        memset(qhdr.MsgTo, ' ', 25);
        memcpy(qhdr.MsgTo, ui->toEdit->text().toStdString().c_str(), (ui->toEdit->text().length() < 25 ? ui->toEdit->text().length() : 25));

        if (qwkesupport && ui->toEdit->text().length() > 25) {
            qwkestr += "To: " + ui->toEdit->text() + "\n";
        }

        memset(qhdr.MsgFrom, ' ', 25);
        memcpy(qhdr.MsgFrom, username.toStdString().c_str(), (username.length() < 25 ? username.length() : 25));

        if (qwkesupport && username.length() > 25) {
            qwkestr += "From: " + username + "\n";
        }

        memset(qhdr.MsgSubj, ' ', 25);
        memcpy(qhdr.MsgSubj, ui->subjEdit->text().toStdString().c_str(), (ui->subjEdit->text().length() < 25 ? ui->subjEdit->text().length() : 25));

        if (qwkesupport && ui->subjEdit->text().length() > 25) {
            qwkestr += "Subject: " + ui->subjEdit->text() + "\n";
        }


        memset(qhdr.Msgpass, ' ', 12);

        qhdr.Msglive = 0xE1;
        qhdr.Msgoffhi = 0;
        qhdr.Msgofflo = 0;
        qhdr.Msgarealo = areano & 0xff;
        qhdr.Msgareahi = (areano >> 8) & 0xff;

        QDate now = QDate::currentDate();
        snprintf(buffer, 64, "%02d-%02d-%02d", now.month(), now.day(), now.year() - 2000);
        memcpy(qhdr.Msgdate, buffer, 8);

        QTime tnow = QTime::currentTime();
        snprintf(buffer, 64, "%02d:%02d", tnow.hour(), tnow.minute());
        memcpy(qhdr.Msgtime, buffer, 5);

        memset(buffer, '\0', 64);
        snprintf(buffer, 7, "%d", areano);
        memset(qhdr.Msgnum, ' ', 7);
        memcpy(qhdr.Msgnum, buffer, strlen(buffer));

        if (replyid != -1) {
            memset(buffer, '\0', 64);
            snprintf(buffer, 8, "%d", replyid);
            memset(qhdr.Msgrply, ' ', 8);
            memcpy(qhdr.Msgrply, buffer, strlen(buffer));
        } else {
            memset(qhdr.Msgrply, ' ', 8);
        }

        QString msgbody = "";

        if (qwkesupport && qwkestr.length() > 0) {
            msgbody += qwkestr + "\n";
        }

        if (msgtag == "") {
            msgbody += ui->msgEdit->toPlainText() + "\n\n=== TitanMail/" + QSysInfo::kernelType() + " v" + VERSION + "\n";
        } else {
            msgbody += ui->msgEdit->toPlainText() + "\n\n... " + msgtag + "\n=== TitanMail/" + QSysInfo::kernelType() + " v" + VERSION + "\n";
        }

        int msgrecs = msgbody.length() / 128;
        if (msgrecs * 128 < msgbody.length()) {
            msgrecs += 1;
        }

        int msgbytes = msgrecs * 128;

        char *msgbodybytes = (char *)malloc(msgbytes);

        memset(msgbodybytes, ' ', msgbytes);
        for (int i=0;i<msgbody.length();i++) {
            if (msgbody.at(i) == '\n') {
                msgbodybytes[i] = '\xe3';
            } else {
                msgbodybytes[i] = QwkPacket::convert_cp437(msgbody.toStdString()).at(i);
            }
        }
        memset(buffer, '\0', 64);
        snprintf(buffer, 7, "%d", msgrecs + 1);
        memset(qhdr.Msgrecs, ' ', 7);
        memcpy(qhdr.Msgrecs, buffer, strlen(buffer));
        file.write((const char *)&qhdr, sizeof(struct QwkHeader));
        file.write(msgbodybytes, msgbytes);

        free(msgbodybytes);
        file.close();
    }
    close();
}

void NewMsg::on_pushButton_clicked()
{
    close();
}


void NewMsg::on_pushButton_2_clicked()
{
    if (mw->tagline_file != "") {
        TaglineSelect *tls = new TaglineSelect(this, mw->tagline_file);
        tls->show();
    } else {
        addMessage("");
    }
}

