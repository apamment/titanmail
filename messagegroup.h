#ifndef MESSAGEGROUP_H
#define MESSAGEGROUP_H

#include <QString>
#include <QList>
#include "message.h"

class messagegroup
{
public:
    messagegroup(int no);
    ~messagegroup();
    int groupno;
    QString groupname;
    QList<Message *> *msgs;
    int personal;
    int unread;
};

#endif // MESSAGEGROUP_H
