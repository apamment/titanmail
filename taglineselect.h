#ifndef TAGLINESELECT_H
#define TAGLINESELECT_H

#include <QDialog>

class NewMsg;

namespace Ui {
class TaglineSelect;
}

class TaglineSelect : public QDialog
{
    Q_OBJECT

public:
    explicit TaglineSelect(QWidget *parent = nullptr, QString taglinefile = "");
    ~TaglineSelect();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    NewMsg *nmw;
    Ui::TaglineSelect *ui;
};

#endif // TAGLINESELECT_H
