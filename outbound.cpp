#include <QStandardPaths>
#include <QDirIterator>
#include "mainwindow.h"
#include "outbound.h"
#include "ui_outbound.h"
#include "qwkheader.h"
#include "editoutbound.h"

static int safe_atoi(const char *str, int len) {
    int ret = 0;

    for (int i=0;i<len;i++) {
        if (str[i] < '0' || str[i] > '9') {
            break;
        }
        ret = ret * 10 + (str[i] - '0');
    }
    return ret;
}

static QString rstrip(const QString& str) {
  int n = str.size() - 1;
  for (; n >= 0; --n) {
    if (!str.at(n).isSpace()) {
      return str.left(n + 1);
    }
  }
  return "";
}


Outbound::Outbound(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Outbound)
{
    ui->setupUi(this);
    mw = (MainWindow *)parent;
}

Outbound::~Outbound()
{
    delete ui;
}

bool Outbound::read_message(QString filename, struct ob_msg *obmsg) {
    FILE *fptr = fopen(filename.toStdString().c_str(), "rb");
    struct QwkHeader qhdr;
    if (!fptr) {
        return false;
    }

    if (fread(&qhdr, sizeof(struct QwkHeader), 1, fptr) != 1) {
            return false;
    }

    obmsg->hdr = qhdr;

    int msg_recs = safe_atoi((const char *)qhdr.Msgrecs, 6);
    char *msg_body = (char *)malloc(((msg_recs -1) * 128) + 1);
    if (!msg_body) {
        fclose(fptr);
        return false;
    }

    memset(msg_body, 0, (msg_recs - 1) * 128 + 1);
    fread(msg_body, sizeof(struct QwkHeader), msg_recs - 1, fptr);
    for (size_t i = 0; i < strlen(msg_body); i++) {
        if (msg_body[i] == '\xe3') {
            msg_body[i] = '\n';
        }
    }
    fclose(fptr);
    obmsg->subj = QString((char *)qhdr.MsgSubj).left(25).trimmed();
    obmsg->from = QString((char *)qhdr.MsgFrom).left(25).trimmed();
    obmsg->to = QString((char *)qhdr.MsgTo).left(25).trimmed();
    obmsg->body = rstrip(QString::fromStdString(QwkPacket::convert_utf8(std::string((char *)msg_body))));

    if (mw->qwkesupport) {
        bool gotkludge = false;
        while (true) {
            if (obmsg->body.left(8).toLower() == "subject:") {
                obmsg->subj = "";
                int i;
                for (i=8;i<obmsg->body.length();i++) {
                    if (obmsg->body.at(i) == '\n') {
                        break;
                    }
                    obmsg->subj.append(obmsg->body.at(i));
                }
                obmsg->body.remove(0, i + 1);
                obmsg->subj = obmsg->subj.trimmed();
                gotkludge = true;
            } else if (obmsg->body.left(3).toLower() == "to:") {
                obmsg->to = "";
                int i;
                for (i=3;i<obmsg->body.length();i++) {
                    if (obmsg->body.at(i) == '\n') {
                        break;
                    }
                    obmsg->to.append(obmsg->body.at(i));
                }
                obmsg->body.remove(0, i + 1);
                obmsg->to = obmsg->to.trimmed();
                gotkludge = true;
            } else if (obmsg->body.left(5).toLower() == "from:") {
                obmsg->from = "";
                int i;
                for (i=5;i<obmsg->body.length();i++) {
                    if (obmsg->body.at(i) == '\n') {
                        break;
                    }
                    obmsg->from.append(obmsg->body.at(i));
                }
                obmsg->body.remove(0, i + 1);
                obmsg->from = obmsg->from.trimmed();
                gotkludge = true;
            } else {
                if (gotkludge && obmsg->body.at(0) == '\n') {
                    obmsg->body.remove(0, 1);
                }
                break;
            }
        }
    }

    obmsg->body.replace(QRegularExpression("\\x1B\\[[0-9|;|?]*[A-Z|a-z]"), "");

    free(msg_body);
    obmsg->filename = filename;
    return true;
}

bool Outbound::load() {
    QString SavePath = QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + mw->qwkpacket->bbsqwkid + "/");
    ui->listWidget->clear();

    messages.clear();

    QDirIterator it(SavePath, QStringList() << "msg.*");

    while (it.hasNext()) {
        it.next();

        struct ob_msg msg;


        if (read_message(it.filePath(), &msg)) {
            messages.push_back(msg);
            int areano = 0xfffff & ((msg.hdr.Msgareahi << 8) | msg.hdr.Msgarealo);
            QString group = "";
            for (int i = 0; i < mw->qwkpacket->groups->count(); i++) {
                if (mw->qwkpacket->groups->at(i)->groupno == areano) {
                    group = mw->qwkpacket->groups->at(i)->groupname;
                    break;
                }
            }


            ui->listWidget->addItem("[" + group + "] [To: " + msg.to + "] " + msg.subj);
        }

    }
    if (messages.size() > 0) {
        return true;
    }

    return false;
}

void Outbound::on_deleteButton_clicked()
{
    if (ui->listWidget->selectedItems().count() > 0) {
        QDir().remove(messages.at(ui->listWidget->selectionModel()->selectedIndexes().first().row()).filename);
        if (!load()) {
            close();
        }
    }
}


void Outbound::on_editButton_clicked()
{
    if (ui->listWidget->selectedItems().count() > 0) {
        EditOutbound eob(this, mw, &messages.at(ui->listWidget->selectionModel()->selectedIndexes().first().row()));
        eob.exec();
    }
}

