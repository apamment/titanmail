#include <QProcess>
#include <QTemporaryDir>
#include <QRegularExpression>
#include <QStandardPaths>
#include <quazip/JlCompress.h>
#include <sstream>
#include <iostream>
#ifdef _MSC_VER
#include <Windows.h>
#else
#include <iconv.h>
#endif
#include "message.h"
#include "qwkpacket.h"
#include "qwkheader.h"

static int safe_atoi(const char *str, int len) {
    int ret = 0;

    for (int i=0;i<len;i++) {
        if (str[i] < '0' || str[i] > '9') {
            break;
        }
        ret = ret * 10 + (str[i] - '0');
    }
    return ret;
}

static QString rstrip(const QString& str) {
  int n = str.size() - 1;
  for (; n >= 0; --n) {
    if (!str.at(n).isSpace()) {
      return str.left(n + 1);
    }
  }
  return "";
}

std::string QwkPacket::convert_utf8(std::string input) {
  std::string output;

#ifdef _MSC_VER
  int wchars_num = MultiByteToWideChar(437, 0, input.c_str(), -1, NULL, 0);
  wchar_t *wstr = new wchar_t[wchars_num];
  MultiByteToWideChar(437, 0, input.c_str(), -1, wstr, wchars_num);

  int chars_num = WideCharToMultiByte(CP_UTF8, 0, wstr, wchars_num, NULL, 0, NULL, NULL);

  char *str = new char[chars_num + 1];
  memset(str, 0, chars_num + 1);

  WideCharToMultiByte(CP_UTF8, 0, wstr, wchars_num, str, chars_num, NULL, NULL);

  output = std::string(str);

  delete[] str;
  delete[] wstr;
#else
  iconv_t ic;
  ic = iconv_open("UTF-8", "CP437");
  if (ic == (iconv_t)-1) {
    return input;
  }

  int i = 1;

  char *str = new char[input.size() + 1];

  char *inp = (char *)input.c_str();
  size_t isz = input.size();

  char *oup = str;
  size_t osz = input.size();

  memset(str, 0, osz + 1);

#if defined(__GLIBC__) || defined(__HAIKU__) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__APPLE__)
  while (iconv(ic, &inp, &isz, &oup, &osz) == -1) {
#else
  while (iconv(ic, (const char **)&inp, &isz, &oup, &osz) == -1) {
#endif
    if (errno == E2BIG) {
      delete[] str;
      i++;
      str = new char[input.size() * i + 1];
      memset(str, 0, input.size() * i + 1);
      osz = input.size() * i;
      oup = str;
      inp = (char *)input.c_str();
      isz = input.size();
      continue;
    } else {
      output = input;
      iconv_close(ic);
      delete[] str;
      return output;
    }
  }

  output = str;

  iconv_close(ic);

  delete[] str;

#endif
  return output;
}


std::string QwkPacket::convert_cp437(std::string input) {
  std::string output;

#ifdef _MSC_VER
  int wchars_num = MultiByteToWideChar(CP_UTF8, 0, input.c_str(), -1, NULL, 0);
  wchar_t *wstr = new wchar_t[wchars_num];
  MultiByteToWideChar(CP_UTF8, 0, input.c_str(), -1, wstr, wchars_num);

  int chars_num = WideCharToMultiByte(437, 0, wstr, wchars_num, NULL, 0, NULL, NULL);

  char *str = new char[chars_num + 1];
  memset(str, 0, chars_num + 1);

  WideCharToMultiByte(437, 0, wstr, wchars_num, str, chars_num, NULL, NULL);

  output = std::string(str);

  delete[] str;
  delete[] wstr;
#else
  iconv_t ic;
  ic = iconv_open("CP437//TRANSLIT", "UTF-8");
  if (ic == (iconv_t)-1) {
    return input;
  }

  int i = 1;

  char *str = new char[input.size() + 1];

  char *inp = (char *)input.c_str();
  size_t isz = input.size();

  char *oup = str;
  size_t osz = input.size();

  memset(str, 0, osz + 1);
#if defined(__GLIBC__) || defined(__HAIKU__) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__APPLE__)
  while (iconv(ic, &inp, &isz, &oup, &osz) == -1) {
#else
  while (iconv(ic, (const char **)&inp, &isz, &oup, &osz) == -1) {
#endif
    if (errno == E2BIG) {
      delete[] str;
      i++;
      str = new char[input.size() * i + 1];
      memset(str, 0, input.size() * i + 1);
      osz = input.size() * i;
      oup = str;
      inp = (char *)input.c_str();
      isz = input.size();
      continue;
    } else {
      output = input;
      iconv_close(ic);
      delete[] str;
      return output;
    }
  }

  output = str;
  iconv_close(ic);

  delete[] str;

#endif
  return output;
}

QString QwkPacket::getLastError()
{
    return lastError;
}

QwkPacket::QwkPacket()
{
    groups = new QList<messagegroup *>();
}

QwkPacket::~QwkPacket()
{
    delete groups;
}

bool QwkPacket::unpack_zip(QString filename) {
    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(pth + "/_work");

    if (dir.exists()) {
        dir.removeRecursively();
    }

    QDir().mkpath(dir.path());


    JlCompress::extractDir(filename, dir.absolutePath());

    return true;
}

bool QwkPacket::unpack(QString filename, QString unarchive_cmd) {
    if (filename.right(4).toUpper() == ".QWK") {
        QProcess unarc;
        QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

        QDir dir(pth + "/_work");

        if (dir.exists()) {
            dir.removeRecursively();
        }

        QDir().mkpath(dir.path());

        QStringList list = unarchive_cmd.split(QRegularExpression("\\s+"));
        QString cmd = list.first();
        QStringList args;

        for (int z = 1; z < list.count();z++) {
            if (list.at(z) == "$DIRECTORY") {
                args << dir.path();
            } else if (list.at(z) == "$ARCHIVE") {
                args << filename;
            } else {
                args << list.at(z);
            }
        }


        if (unarc.execute(cmd, args) != 0) {
            lastError = "Failed to unzip QWK packet. ";
            return false;
        }
    }
    return true;
}

void QwkPacket::update_read(uint32_t offset, bool read) {
    FILE *fptr;
    char c;

    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir dir(pth + "/_work");

    if (!dir.exists()) {
        return;
    }

    if (dir.exists("messages.dat")) {
        fptr = fopen(dir.filePath("messages.dat").toStdString().c_str(), "r+b");
    } else {
        fptr = fopen(dir.filePath("MESSAGES.DAT").toStdString().c_str(), "r+b");
    }
    if (!fptr) {
        lastError = "Failed to open messages.dat";
        return;
    }

    fseek(fptr, offset, SEEK_SET);

    if (read) {
        c = '-';
    } else {
        c = ' ';
    }

    fwrite(&c, 1, 1, fptr);
    fclose(fptr);
}

struct character_t {
  char c;
  int fg_color;
  int bg_color;
  bool bold;
};

std::vector<std::string> demangle_ansi(const char *msg, size_t len) {
  std::vector<std::string> new_msg;
  int lines = 0;
  int line_at = 0;
  int col_at = 0;
  int params[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  int param_count = 0;
  int fg_color = 7;
  int bg_color = 0;
  bool bold = false;
  int save_col = 0;
  int save_row = 0;

  if (msg == NULL || len == 0) {
    return new_msg;
  }

  for (size_t i = 0; i < len; i++) {
    if (msg[i] == '\r' || (i >= 1 && msg[i] == '\n' && msg[i - 1] != '\r')) {
      line_at++;
      if (line_at > lines) {
        lines = line_at;
      }
      col_at = 0;
    } else if (msg[i] == '\x1b') {
      i++;
      if (msg[i] != '[') {
        i--;
        continue;
      } else {
        param_count = 0;
        while (i < len && strchr("ABCDEFGHIGJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", msg[i]) == NULL) {
          if (msg[i] == ';') {
            param_count++;
          } else if (msg[i] >= '0' && msg[i] <= '9') {
            if (param_count == 0) {
              param_count = 1;
              for (int j = 0; j < 9; j++) {
                params[j] = 0;
              }
            }
            params[param_count - 1] = params[param_count - 1] * 10 + (msg[i] - '0');
          }
          i++;
        }
        switch (msg[i]) {
        case 'A':
          if (param_count > 0) {
            line_at -= params[0];
          } else {
            line_at--;
          }
          if (line_at < 0)
            line_at = 0;
          break;
        case 'B':
          if (param_count > 0) {
            line_at += params[0];
          } else {
            line_at++;
          }
          if (line_at > lines) {
            lines = line_at;
          }
          break;
        case 'C':
          if (param_count > 0) {
            col_at += params[0];
          } else {
            col_at++;
          }
          if (col_at > 80) {
            col_at = 80;
          }
          break;
        case 'D':
          if (param_count > 0) {
            col_at -= params[0];
          } else {
            col_at--;
          }
          if (col_at < 0)
            col_at = 0;
          break;
        case 'H':
        case 'f':
          if (param_count > 1) {
            params[0]--;
            params[1]--;
          }
          line_at = params[0];
          col_at = params[1];

          if (line_at > lines) {
            lines = line_at;
          }
          if (col_at > 80) {
            col_at = 80;
          }
          if (line_at < 0)
            line_at = 0;
          if (col_at < 0)
            col_at = 0;
          break;
        case 'u':
          col_at = save_col;
          line_at = save_row;
          break;
        case 's':
          save_col = col_at;
          save_row = line_at;
          break;
        case 'm':
          break;
        default:

          break;
        }
      }
    } else if (msg[i] != '\n') {
      col_at++;

      if (col_at >= 80) {
        col_at = 0;
        line_at++;
        if (line_at > lines) {
          lines = line_at;
        }
      }
    }
  }

  struct character_t **fakescreen = (struct character_t **)malloc(sizeof(struct character_t *) * (lines + 1));

  if (!fakescreen) {
    return new_msg;
  }

  for (int i = 0; i <= lines; i++) {
    fakescreen[i] = (struct character_t *)malloc(sizeof(struct character_t) * (80 + 1));
    if (!fakescreen[i]) {
      for (int j = i - 1; j >= 0; j--) {
        free(fakescreen[j]);
      }
      free(fakescreen);
      return new_msg;
    }
    for (size_t x = 0; x <= 80; x++) {
      fakescreen[i][x].c = ' ';
      fakescreen[i][x].fg_color = 7;
      fakescreen[i][x].bg_color = 0;
    }
  }
  line_at = 0;
  col_at = 0;
  save_row = 0;
  save_col = 0;
  for (size_t i = 0; i < len; i++) {
    if (msg[i] == '\r' || (i >= 1 && msg[i] == '\n' && msg[i - 1] != '\r')) {
      line_at++;
      col_at = 0;
    } else if (msg[i] == '\x1b') {
      i++;
      if (msg[i] != '[') {
        i--;
        continue;
      } else {
        param_count = 0;
        while (i < len && strchr("ABCDEFGHIGJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", msg[i]) == NULL) {
          if (msg[i] == ';') {
            param_count++;
          } else if (msg[i] >= '0' && msg[i] <= '9') {
            if (param_count == 0) {
              param_count = 1;
              for (int j = 0; j < 9; j++) {
                params[j] = 0;
              }
            }
            params[param_count - 1] = params[param_count - 1] * 10 + (msg[i] - '0');
          }
          i++;
        }
        switch (msg[i]) {
        case 'A':
          if (param_count > 0) {
            line_at -= params[0];
          } else {
            line_at--;
          }
          if (line_at < 0)
            line_at = 0;
          break;
        case 'B':
          if (param_count > 0) {
            line_at += params[0];
          } else {
            line_at++;
          }
          break;
        case 'C':
          if (param_count > 0) {
            col_at += params[0];
          } else {
            col_at++;
          }
          if (col_at > 80) {
            col_at = 80;
          }
          break;
        case 'D':
          if (param_count > 0) {
            col_at -= params[0];
          } else {
            col_at--;
          }
          if (col_at < 0)
            col_at = 0;
          break;
        case 'H':
        case 'f':
          if (param_count > 1) {
            params[0]--;
            params[1]--;
          }
          line_at = params[0];
          col_at = params[1];
          if (line_at < 0)
            line_at = 0;
          if (col_at < 0)
            col_at = 0;
          if (col_at > 80)
            col_at = 80;
          break;
        case 'm':
          for (int z = 0; z < param_count; z++) {
            if (params[z] == 0) {
              bold = false;
              fg_color = 7;
              bg_color = 0;
            } else if (params[z] == 1) {
              bold = true;
            } else if (params[z] == 2) {
              bold = false;
            }

            else if (params[z] >= 30 && params[z] <= 37) {
              fg_color = params[z] - 30;
            } else if (params[z] >= 40 && params[z] <= 47) {
              bg_color = params[z] - 40;
            }
          }
          break;
        case 'u':
          col_at = save_col;
          line_at = save_row;
          break;
        case 's':
          save_col = col_at;
          save_row = line_at;
          break;
        }
      }
    } else if (msg[i] != '\n') {
      fakescreen[line_at][col_at].c = msg[i];
      fakescreen[line_at][col_at].bold = bold;
      fakescreen[line_at][col_at].fg_color = fg_color;
      fakescreen[line_at][col_at].bg_color = bg_color;
      col_at++;
      if (col_at >= 80) {
        line_at++;
        col_at = 0;
      }
    }
  }

  for (int i = 0; i < lines; i++) {
    for (int j = 80 - 1; j >= 0; j--) {
      if (fakescreen[i][j].c == ' ') {
        fakescreen[i][j].c = '\0';
      } else {
        break;
      }
    }
  }

  std::stringstream ss;

  fg_color = 7;
  bg_color = 0;
  bold = false;
  bool got_tearline = false;
  for (int i = 0; i < lines; i++) {
    ss.str("");
    size_t j;

    if ((fakescreen[i][0].c == '-' && fakescreen[i][1].c == '-' && fakescreen[i][2].c == '-') && (fakescreen[i][3].c == 0 || fakescreen[i][3].c == ' ')) {
      got_tearline = true;
    }

    if (!got_tearline) {
          if (fakescreen[i][0].c != '\001') {
            ss << "<span style=\"";
            ss << "color: ";
            switch (fg_color) {
            case 0:
                if (!bold) {
                    ss << "#000000";
                } else {
                    ss << "#555555";
                }
                break;
            case 1:
                if (!bold) {
                    ss << "#aa0000";
                } else {
                    ss << "#ff5555";
                }
                break;
            case 2:
                if (!bold) {
                    ss << "#00aa00";
                } else {
                    ss << "#55ff55";
                }
                break;
            case 3:
                if (!bold) {
                    ss << "#aaaa00";
                } else {
                    ss << "#ffff55";
                }
                break;
            case 4:
                if (!bold) {
                    ss << "#0000aa";
                } else {
                    ss << "#5555ff";
                }
                break;
            case 5:
                if (!bold) {
                    ss << "#aa00aa";
                } else {
                    ss << "#ff55ff";
                }
                break;
            case 6:
                if (!bold) {
                    ss << "#00aaaa";
                } else {
                    ss << "#55ffff";
                }
                break;
            case 7:
                if (!bold) {
                    ss << "#aaaaaa";
                } else {
                    ss << "#ffffff";
                }
                break;

            }
            ss << " background-color: ";
            switch (bg_color) {
            case 0:
                ss << "#000000";
                break;
            case 1:
                ss << "#aa0000";
                break;
            case 2:
                ss << "#00aa00";
                break;
            case 3:
                ss << "#aaaa00";
                break;
            case 4:
                ss << "#0000aa";
                break;
            case 5:
                ss << "#aa00aa";
                break;
            case 6:
                ss << "#00aaaa";
                break;
            case 7:
                ss << "#aaaaaa";
                break;
            }
            ss << "\">";
          }

    }
    for (j = 0; j < 80; j++) {
      if (fakescreen[i][j].c == '\0') {
        break;
      }
      if (!got_tearline) {
            bool resett = false;
            if (fakescreen[i][j].bold != bold) {
              bold = fakescreen[i][j].bold;
              if (!bold) {
                resett = true;
              }
            }
            bool dospan = false;
            if (fakescreen[i][j].fg_color != fg_color || fakescreen[i][j].bg_color != bg_color || resett) {
              ss << "</span><span style=\"";
              fg_color = fakescreen[i][j].fg_color;
              ss << "color: ";
              switch (fg_color) {
              case 0:
                  if (!bold) {
                      ss << "#000000";
                  } else {
                      ss << "#555555";
                  }
                  break;
              case 1:
                  if (!bold) {
                      ss << "#aa0000";
                  } else {
                      ss << "#ff5555";
                  }
                  break;
              case 2:
                  if (!bold) {
                      ss << "#00aa00";
                  } else {
                      ss << "#55ff55";
                  }
                  break;
              case 3:
                  if (!bold) {
                      ss << "#aaaa00";
                  } else {
                      ss << "#ffff55";
                  }
                  break;
              case 4:
                  if (!bold) {
                      ss << "#0000aa";
                  } else {
                      ss << "#5555ff";
                  }
                  break;
              case 5:
                  if (!bold) {
                      ss << "#aa00aa";
                  } else {
                      ss << "#ff55ff";
                  }
                  break;
              case 6:
                  if (!bold) {
                      ss << "#00aaaa";
                  } else {
                      ss << "#55ffff";
                  }
                  break;
              case 7:
                  if (!bold) {
                      ss << "#aaaaaa";
                  } else {
                      ss << "#ffffff";
                  }
                  break;

              }
              ss << "; background-color: ";
              switch (bg_color) {
              case 0:
                  ss << "#000000";
                  break;
              case 1:
                  ss << "#aa0000";
                  break;
              case 2:
                  ss << "#00aa00";
                  break;
              case 3:
                  ss << "#aaaa00";
                  break;
              case 4:
                  ss << "#0000aa";
                  break;
              case 5:
                  ss << "#aa00aa";
                  break;
              case 6:
                  ss << "#00aaaa";
                  break;
              case 7:
                  ss << "#aaaaaa";
                  break;
              }
              ss << "\">";
            }
          }

      ss << fakescreen[i][j].c;
    }

    ss << "</span><br />";
    new_msg.push_back(QwkPacket::convert_utf8(ss.str()));
  }

  for (int i = 0; i <= lines; i++) {
    free(fakescreen[i]);
  }
  free(fakescreen);

  return new_msg;
}

bool QwkPacket::load_qwk(bool qwkesupport) {
    FILE *fptr;
    struct QwkHeader qhdr;
    char buffer[256];


    QString pth  = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    QDir dir(pth + "/_work");

    if (!dir.exists()) {
        return false;
    }

    fptr = fopen(dir.filePath("control.dat").toStdString().c_str(), "r");
    if (!fptr) {
        fptr = fopen(dir.filePath("CONTROL.DAT").toStdString().c_str(), "r");
        if (!fptr) {
            lastError = "Failed to open control.dat";
            return false;
        }
    }


    fgets(buffer, sizeof buffer, fptr);
    bbsname = QString(buffer).trimmed();

    fgets(buffer, sizeof buffer, fptr);
    bbslocation = QString(buffer).trimmed();

    fgets(buffer, sizeof buffer, fptr); // phone number

    fgets(buffer, sizeof buffer, fptr);
    sysopname = QString(buffer).trimmed();
    fgets(buffer, sizeof buffer, fptr);
    bbsqwkid = QString(strchr(buffer, ',') + 1).trimmed();

    fgets(buffer, sizeof buffer, fptr);
    packetdate = QString(buffer).trimmed();

    fgets(buffer, sizeof buffer, fptr);
    username = QString(buffer).trimmed();
    fgets(buffer, sizeof buffer, fptr);
    fgets(buffer, sizeof buffer, fptr);
    fgets(buffer, sizeof buffer, fptr);
    fgets(buffer, sizeof buffer, fptr);
    int totareas = QString(buffer).trimmed().toInt();
    int count = 0;

    while (!feof(fptr) && count <= totareas) {
        fgets(buffer, sizeof buffer, fptr);
        int area = QString(buffer).trimmed().toInt();
        fgets(buffer, sizeof buffer, fptr);
        messagegroup *gr = new messagegroup(area);
        gr->groupname = QString::number(area) + " - " + QString(buffer).trimmed();
        groups->append(gr);
        count++;
    }

    bulletins.clear();
    fgets(buffer, sizeof buffer, fptr);
    while (!feof(fptr)) {
        if (QString(buffer).trimmed() != "" && dir.exists(QString(buffer).trimmed())) {
            bulletins.push_back(QString(buffer).trimmed());
        }
        fgets(buffer, sizeof buffer, fptr);
    }

    fclose(fptr);

    QStringList directorylist(dir.entryList(QStringList() << "BLT-*", QDir::Files, QDir::Name));

    foreach(QString filename, directorylist) {
        bulletins.push_back(filename);
    }

    fptr = fopen(dir.filePath("messages.dat").toStdString().c_str(), "rb");
    if (!fptr) {
        fptr = fopen(dir.filePath("MESSAGES.DAT").toStdString().c_str(), "rb");
        if (!fptr) {
            lastError = "Failed to open messages.dat";
            return false;
        }
    }

    fread(&qhdr, sizeof(struct QwkHeader), 1, fptr);


    while (!feof(fptr)) {
        uint32_t offset = ftell(fptr);

        if (fread(&qhdr, sizeof(struct QwkHeader), 1, fptr) != 1) {
            break;
        }

        int msg_recs = safe_atoi((const char *)qhdr.Msgrecs, 6);
        char *msg_body = (char *)malloc(((msg_recs -1) * 128) + 1);
        if (!msg_body) {
            lastError = "Failed to allocate memory.";
            fclose(fptr);
            return false;
        }
        memset(msg_body, 0, (msg_recs - 1) * 128 + 1);
        fread(msg_body, sizeof(struct QwkHeader), msg_recs - 1, fptr);
        for (size_t i = 0; i < strlen(msg_body); i++) {
            if (msg_body[i] == '\xe3') {
                msg_body[i] = '\n';
            }
        }

        QString Subject = QString((char *)qhdr.MsgSubj).left(25).trimmed();
        QString From = QString((char *)qhdr.MsgFrom).left(25).trimmed();
        QString To = QString((char *)qhdr.MsgTo).left(25).trimmed();
        QDate date;

        int yr = (qhdr.Msgdate[6] - '0') * 10 + (qhdr.Msgdate[7] - '0');

        if (yr > 79) {
            yr += 1900;
        } else {
            yr += 2000;
        }

        int mon = (qhdr.Msgdate[0] - '0') * 10 + (qhdr.Msgdate[1] - '0');

        int day = (qhdr.Msgdate[3] - '0') * 10 + (qhdr.Msgdate[4] - '0');

        date.setDate(yr, mon, day);
        QTime time;

        int hour = (qhdr.Msgtime[0] - '0') * 10 + (qhdr.Msgtime[1] - '0');
        int min = (qhdr.Msgtime[3] - '0') * 10 + (qhdr.Msgtime[4] - '0');
        time.setHMS(hour, min, 0);

        QDateTime dt;
        dt.setDate(date);
        dt.setTime(time);



        std::vector<std::string> nohtmlmsg;
        bool is_ansi_msg = false;

        std::stringstream ss;

        for (int i = 0; i < strlen(msg_body);i++) {
            if (msg_body[i] == '\x1b') is_ansi_msg = true;
            if (msg_body[i] == '\n') {
                nohtmlmsg.push_back(ss.str());
                ss.str("");
            } else {
                ss << msg_body[i];
            }
        }


        std::stringstream body_str;


        for (size_t p = 0; p < nohtmlmsg.size(); p++) {
            std::string nohtml = nohtmlmsg.at(p);
            if (qwkesupport) {
                if (QString::fromStdString(nohtml).left(8).toLower() == "subject:") {
                    Subject = QString::fromStdString(nohtml).mid(8).trimmed();
                } else if (QString::fromStdString(nohtml).left(3).toLower() == "to:") {
                    To = QString::fromStdString(nohtml).mid(3).trimmed();
                } else if (QString::fromStdString(nohtml).left(5).toLower() == "from:") {
                    From = QString::fromStdString(nohtml).mid(5).trimmed();
                } else {
                    body_str << nohtml << "\n";
                }
            } else {
               body_str << nohtml << "\n";
            }
        }

        QString body = QString::fromStdString(convert_utf8(body_str.str()));
        body.replace(QRegularExpression("\\x1B\\[[0-9|;|?]*[A-Z|a-z]"), "");
        QString ansibody = "";
        if (is_ansi_msg) {
            ansibody = "<div style=\"white-space:pre;\">";
            std::vector<std::string> ansimsg = demangle_ansi(body_str.str().c_str(), body_str.str().size());
            for (size_t p = 0; p < ansimsg.size(); p++) {
                ansibody = ansibody + QString::fromStdString(ansimsg.at(p));
            }
            ansibody = ansibody + "</div>";
        }

        if (qhdr.Msgstat != 0x56) { // Don't add Synchronet "Voting" messages.
            Message *msg  = new Message(QString((char *)qhdr.Msgnum).left(7).trimmed().toInt(), Subject, From, To, dt, body, ansibody);

            msg->offset = offset;
            if (qhdr.Msgstat == '-') {
                msg->read = true;
            } else {
                msg->read = false;
            }

            if (msg->to.toLower() == username.toLower()) {
                msg->personal = true;
            } else {
                msg->personal = false;
            }

            int areano = 0xfffff & ((qhdr.Msgareahi << 8) | qhdr.Msgarealo);
            bool found = false;
            for (int i = 0; i< groups->count(); i++) {
                if (groups->at(i)->groupno == areano) {
                    found = true;
                    groups->at(i)->msgs->append(msg);
                    if (msg->personal) {
                        groups->at(i)->personal++;
                    }
                    if (!msg->read) {
                        groups->at(i)->unread++;
                    }
                    break;
                }
            }

            if (!found) {
                messagegroup *mg = new messagegroup(areano);
                mg->msgs->append(msg);
                if (msg->personal) {
                    mg->personal++;
                }
                groups->append(mg);
            }
        }
        free(msg_body);
    }

    fclose(fptr);

    return true;
}
