#ifndef FETCHDIALOG_H
#define FETCHDIALOG_H

#include <QDialog>
#include <QtNetwork/QNetworkReply>

class MainWindow;

namespace Ui {
class FetchDialog;
}

class FetchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FetchDialog(QWidget *parent = nullptr, bool down = true);
    ~FetchDialog();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    MainWindow *mw;
    Ui::FetchDialog *ui;
    bool up_or_down;
};

#endif // FETCHDIALOG_H
