#include <QSaveFile>
#include <QStandardPaths>
#include <QSettings>
#include <QDir>
#include <curl/curl.h>
#include "mainwindow.h"
#include "fetchdialog.h"
#include "ui_fetchdialog.h"

FetchDialog::FetchDialog(QWidget *parent, bool down) :
    QDialog(parent),
    ui(new Ui::FetchDialog)
{
    ui->setupUi(this);
    this->mw = (MainWindow *)parent;

    up_or_down = down;

    QSettings settings("Andrew Pamment", "TitanMail");
    ui->ftp_server->setText(settings.value("ftp_server", "vert.synchro.net").toString());
    ui->ftp_port->setText(settings.value("ftp_port", "21").toString());
    ui->ftp_file->setText(settings.value("ftp_file", "VERT").toString());
    ui->ftp_username->setText(settings.value("ftp_username", "").toString());
    ui->ftp_password->setText(settings.value("ftp_password", "").toString());

    if (down) {
        ui->ftp_file->setText(ui->ftp_file->text() + ".QWK");
    } else {
        ui->ftp_file->setText(ui->ftp_file->text() + ".REP");
    }
}

FetchDialog::~FetchDialog()
{
    delete ui;
}

static size_t write_callback(char *ptr, size_t size, size_t nmemb, void *stream) {
  size_t retcode = fwrite(ptr, size, nmemb, (FILE *)stream);

  return retcode;
}

static size_t read_callback(char *ptr, size_t size, size_t nmemb, void *stream) {
  size_t retcode = fread(ptr, size, nmemb, (FILE *)stream);

  return retcode;
}

void FetchDialog::on_buttonBox_accepted()
{
    mw->setEnabled(false);
    QSettings settings("Andrew Pamment", "TitanMail");
    settings.setValue("ftp_server", ui->ftp_server->text());
    settings.setValue("ftp_port", ui->ftp_port->text());
    settings.setValue("ftp_username", ui->ftp_username->text());
    settings.setValue("ftp_password", ui->ftp_password->text());

    QString file = ui->ftp_file->text();

    if (up_or_down) {
        if (!file.endsWith(".QWK", Qt::CaseInsensitive)) {
            file.append(".QWK");
        }
    } else {
        if (!file.endsWith(".REP", Qt::CaseInsensitive)) {
            file.append(".REP");
        }
    }


    curl_global_init(CURL_GLOBAL_DEFAULT);
    CURL *curl = curl_easy_init();
    if(curl) {
        QString url = "ftp://" + ui->ftp_username->text() + ":" + ui->ftp_password->text() + "@" + ui->ftp_server->text() + ":" + ui->ftp_port->text() + "/" + file;
        file.truncate(file.length() - 4);
        settings.setValue("ftp_file", file);
        if (up_or_down) {
            QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
            if (!dir.exists()) {
                QDir().mkpath(dir.path());
            }

            FILE *fptr = fopen(std::string(dir.path().toStdString() + "/temp.qwk").c_str(), "wb");
            if (!fptr) {
                this->mw->ftpDownloadFinished(false, "Unable to open file for writing");
                return;
            }
            curl_easy_setopt(curl, CURLOPT_URL, url.toStdString().c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, fptr);

            CURLcode res = curl_easy_perform(curl);

            if (res != CURLE_OK) {
              fclose(fptr);
              // failed
              QFile f(dir.path() + "/temp.qwk");
              if (f.exists()) {
                  f.remove();
              }
              this->mw->ftpDownloadFinished(false, QString(curl_easy_strerror(res)));
            } else {
              // success
              fclose(fptr);
              this->mw->ftpDownloadFinished(true);
            }

        } else {
            QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
            if (!dir.exists()) {
                this->mw->ftpUploadFinished(false, "Rep file does not exist");
                return;
            }

            FILE *fptr = fopen(std::string(dir.path().toStdString() + "/temp.rep").c_str(), "rb");
            if (!fptr) {
                this->mw->ftpUploadFinished(false, "Unable to open rep file");
                return;
            }
            curl_easy_setopt(curl, CURLOPT_URL, url.toStdString().c_str());
            curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
            curl_easy_setopt(curl, CURLOPT_READDATA, fptr);
            curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
            CURLcode res = curl_easy_perform(curl);

            if (res != CURLE_OK) {
              fclose(fptr);
              this->mw->ftpUploadFinished(false, QString(curl_easy_strerror(res)));
            } else {
              // success
              fclose(fptr);
              this->mw->ftpUploadFinished(true);
            }

        }
    }

}



void FetchDialog::on_buttonBox_rejected()
{

}

