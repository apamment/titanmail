#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include <QDirIterator>
#include <QProcess>
#include <QSettings>
#include <sstream>
#include <quazip/JlCompress.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newmsg.h"
#include "qwkheader.h"
#include "about.h"
#include "settings.h"
#include "fetchdialog.h"
#include "outbound.h"
#include "bulletin.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qwkpacket = nullptr;
    ui->msgTable->setColumnCount(5);
    ui->msgTable->setRowCount(0);
    ui->msgTable->setHorizontalHeaderLabels(QStringList() << "MsgNo" << "Subject" << "From" << "To" << "Date");
    ui->areaList->setColumnCount(4);
    ui->areaList->setHorizontalHeaderItem(0, new QTableWidgetItem("Group name"));
    ui->areaList->setHorizontalHeaderItem(1, new QTableWidgetItem(QIcon(":/mail-personal.svg"), "Personal"));
    ui->areaList->setHorizontalHeaderItem(2, new QTableWidgetItem(QIcon(":/mail-new.svg"), "Unread"));
    ui->areaList->setHorizontalHeaderItem(3, new QTableWidgetItem(QIcon(":/mail.svg"), "Total"));

    ui->areaList->setRowCount(0);

    selected_msg = -1;
    selected_group = -1;
    QSettings settings("Andrew Pamment", "TitanMail");
    unarchive_cmd = settings.value("unarchive_cmd", "unzip -jLoq -d $DIRECTORY $ARCHIVE").toString();
    archive_cmd = settings.value("archive_cmd", "zip -jkq $ARCHIVE $FILENAME").toString();
    signature = settings.value("signature", "").toString();
    msgViewState = settings.value("msg_list_state").toByteArray();
    grpViewState = settings.value("grp_list_state").toByteArray();
    qwkesupport = settings.value("qwke_support", false).toBool();
    editor_font = settings.value("editor_font", ui->msgText->fontFamily()).toString();
    editor_font_size = settings.value("editor_font_size", ui->msgText->fontPointSize()).toReal();
    tagline_file = settings.value("tagline_file", "").toString();
    hide_empty_areas = settings.value("hide_empty_areas", false).toBool();
    intzip = settings.value("internal_zip", true).toBool();
    ui->msgText->setFontFamily(editor_font);
    ui->msgText->setFontPointSize(editor_font_size);
    ui->splitter->restoreState(settings.value("splitter1_state", ui->splitter->saveState()).toByteArray());
    ui->splitter_2->restoreState(settings.value("splitter2_state", ui->splitter_2->saveState()).toByteArray());
    restoreGeometry(settings.value("window_geometry", saveGeometry()).toByteArray());
    ui->actionHide_Empty_Areas->setChecked(hide_empty_areas);

    ui->msgTable->horizontalHeader()->restoreState(msgViewState);
    ui->areaList->horizontalHeader()->restoreState(grpViewState);
    this->setWindowTitle("TitanMail v" VERSION);
}

void MainWindow::load() {
    qwkpacket = new QwkPacket();
    if (!qwkpacket->load_qwk(qwkesupport)) {
        delete qwkpacket;
        qwkpacket = nullptr;
    }
    refresh_area_list();
    if (qwkpacket != nullptr) {
        if (qwkpacket->bulletins.size() > 0) {
            ui->menuBulletins->setEnabled(true);
            ui->menuBulletins->clear();
            for(QString b : qwkpacket->bulletins) {
               ui->menuBulletins->addAction(b, [b, this](bool){showBulletin(b);});
            }
        } else {
            ui->menuBulletins->setEnabled(false);
            ui->menuBulletins->clear();
        }

        if (qwkpacket->bbslocation.length() > 0) {
            ui->BBSNameLabel->setText(qwkpacket->bbsname + ", " + qwkpacket->bbslocation);
        } else {
            ui->BBSNameLabel->setText(qwkpacket->bbsname);
        }
    }
}

MainWindow::~MainWindow()
{
    QSettings settings("Andrew Pamment", "TitanMail");
    settings.setValue("msg_list_state", ui->msgTable->horizontalHeader()->saveState());
    settings.setValue("grp_list_state", ui->areaList->horizontalHeader()->saveState());
    settings.setValue("window_geometry", saveGeometry());
    settings.setValue("splitter1_state", ui->splitter->saveState());
    settings.setValue("splitter2_state", ui->splitter_2->saveState());
    delete ui;
}

void MainWindow::showBulletin(QString b) {
    Bulletin bwin(this);
    bwin.load(b);
    bwin.exec();
}

void MainWindow::openQwkFile(QString fileName) {
    if (qwkpacket != nullptr) {
        msgViewState = ui->msgTable->horizontalHeader()->saveState();
        grpViewState = ui->areaList->horizontalHeader()->saveState();
        delete qwkpacket;
        ui->msgText->setText("");
        ui->msgTable->clear();
        ui->areaList->clear();
        ui->msgTable->setColumnCount(5);
        ui->msgTable->setRowCount(0);
        ui->msgTable->setHorizontalHeaderLabels(QStringList() << "MsgNo" << "Subject" << "From" << "To" << "Date");
        ui->areaList->setColumnCount(4);
        ui->areaList->setRowCount(0);
        ui->areaList->setHorizontalHeaderItem(0, new QTableWidgetItem("Group name"));
        ui->areaList->setHorizontalHeaderItem(1, new QTableWidgetItem(QIcon(":/mail-personal.svg"), "Personal"));
        ui->areaList->setHorizontalHeaderItem(2, new QTableWidgetItem(QIcon(":/mail-new.svg"), "Unread"));
        ui->areaList->setHorizontalHeaderItem(3, new QTableWidgetItem(QIcon(":/mail.svg"), "Total"));
    }


    qwkpacket = new QwkPacket();

    bool res;

    if (intzip) {
        res = qwkpacket->unpack_zip(fileName);
    } else {
        res = qwkpacket->unpack(fileName, unarchive_cmd);
    }

    if (!res) {
        QMessageBox mb(QMessageBox::Icon::Critical, "Error Opening QWK Packet", qwkpacket->getLastError(), QMessageBox::StandardButton::Ok);

        mb.exec();

        delete qwkpacket;
        qwkpacket = nullptr;
    } else {
        if (!qwkpacket->load_qwk(qwkesupport)) {
            QMessageBox mb(QMessageBox::Icon::Critical, "Error Loading QWK Packet", qwkpacket->getLastError(), QMessageBox::StandardButton::Ok);

            mb.exec();

            delete qwkpacket;
            qwkpacket = nullptr;
        } else {
            QString SavePath = QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkpacket->bbsqwkid + "/");
            QDir d(SavePath);
            if (d.exists()) {
                QMessageBox mb(QMessageBox::Icon::Critical, "Existing Replys Found", "There are old Replies saved for this QWK ID, Nuke them?", QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No);

                int ret = mb.exec();
                if (ret == QMessageBox::Yes) {
                    d.removeRecursively();
                }
            }
            if (qwkpacket->bulletins.size() > 0) {
                ui->menuBulletins->setEnabled(true);
                ui->menuBulletins->clear();
                for(QString b : qwkpacket->bulletins) {
                   ui->menuBulletins->addAction(b, [b, this](bool){showBulletin(b);});
                }
            } else {
                ui->menuBulletins->setEnabled(false);
                ui->menuBulletins->clear();
            }
            refresh_area_list();
            if (qwkpacket->bbslocation.length() > 0) {
                ui->BBSNameLabel->setText(qwkpacket->bbsname + ", " + qwkpacket->bbslocation);
            } else {
                ui->BBSNameLabel->setText(qwkpacket->bbsname);
            }
        }
    }
}

void MainWindow::on_actionOpen_QWK_Packet_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open QWK Packet"),
                                                    QStandardPaths::writableLocation(QStandardPaths::HomeLocation),
                                                    tr("QWK Packets (*.qwk *.QWK)"));

    if (fileName == nullptr) {
        return;
    }

    openQwkFile(fileName);
}

void MainWindow::on_msgTable_cellClicked(int row, int column)
{

}

void MainWindow::on_replyButton_clicked()
{
    if (qwkpacket != nullptr && selected_msg != -1 && selected_group != -1) {
        QString quote;

        quote = "-=> On " + qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->date.toString() + ", " + qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->from + " wrote to " + qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->to + " <=-\n\n";
        quote += " > ";
        for (int i=0;i<qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->body.length(); i++) {
            if (qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->body.at(i) == '\n') {
                quote += "\n > ";
            } else {
                quote += qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->body.at(i);
            }
        }

        quote += "\n" + signature;

        NewMsg nm(this, qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->subject, qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->from, quote, qwkpacket->bbsqwkid, qwkpacket->groups->at(selected_group)->groupno, qwkpacket->username, qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->msgno, qwkesupport);
        nm.exec();
    }
}

void MainWindow::on_newMsgButton_clicked()
{
    if (qwkpacket != nullptr && selected_group != -1) {
        NewMsg nm(this, "", "All", signature, qwkpacket->bbsqwkid, qwkpacket->groups->at(selected_group)->groupno, qwkpacket->username, -1, qwkesupport);
        nm.exec();
    }
}

bool MainWindow::createRepPacket(QString fileName) {
    if (qwkpacket != nullptr) {
        QString SavePath = QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkpacket->bbsqwkid + "/");
        QString MsgPath = QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkpacket->bbsqwkid + ".MSG");

        if (fileName == nullptr) {
            return false;
        }

        struct QwkHeader qhdr;
        char buffer[128];

        memset(buffer, ' ', 128);
        memcpy(buffer, qwkpacket->bbsqwkid.toStdString().c_str(), qwkpacket->bbsqwkid.length());

        QDirIterator it(SavePath, QStringList() << "msg.*");

        if (!it.hasNext()) {
            QMessageBox mb(QMessageBox::Icon::Information, "No Outbound Messages", "No messages found to pack.", QMessageBox::StandardButton::Ok);
            mb.exec();
            return false;
        }

        uint16_t i = 0;
        QFile ofile(MsgPath);
        if (ofile.open(QFile::WriteOnly)) {
            ofile.write(buffer, 128);
            while (it.hasNext()) {
                QFile file(it.next());
                if(file.open(QFile::ReadOnly)) {

                    i++;
                    file.read((char *)&qhdr, sizeof(struct QwkHeader));
                    qhdr.Msgofflo = i & 0xff;
                    qhdr.Msgoffhi = (i >> 8) & 0xff;

                    ofile.write((char *)&qhdr, sizeof(struct QwkHeader));
                    QByteArray rest = file.readAll();
                    ofile.write(rest);
                    file.close();
                }
            }
            ofile.close();

            if (intzip) {
                if (!JlCompress::compressFile(fileName, MsgPath)) {
                    QMessageBox mb(QMessageBox::Icon::Critical, "Error Archiving REP Packet", "Something went wrong. :(", QMessageBox::StandardButton::Ok);
                    mb.exec();
                    return false;
                }
            } else {

                QProcess arc;
                QStringList list = archive_cmd.split(QRegularExpression("\\s+"));
                QString cmd = list.first();
                QStringList args;
                for (int z = 1; z < list.count(); z++) {
                    if (list.at(z) == "$ARCHIVE") {
                        args << fileName;
                    } else if (list.at(z) == "$FILENAME") {
                        args << MsgPath;
                    } else {
                        args << list.at(z);
                    }
                }

                if (arc.execute(cmd, args) != 0) {
                    QMessageBox mb(QMessageBox::Icon::Critical, "Error Archiving REP Packet", "Something went wrong. :(", QMessageBox::StandardButton::Ok);
                    mb.exec();
                    return false;
                }
            }
            QFile::remove(MsgPath);

            QMessageBox mb(QMessageBox::Icon::Information, "Created REP Packet", tr("A reply packet with %1 messages has been created.").arg(i), QMessageBox::StandardButton::Ok);
            mb.exec();
            if (i > 0) {
                return true;
            }
        }
    }

    return false;
}

void MainWindow::on_actionSave_REP_Packet_triggered()
{
    if (qwkpacket != nullptr) {
        QString fileName = QFileDialog::getExistingDirectory(this, tr("Save REP Packet"),
        QStandardPaths::writableLocation(QStandardPaths::HomeLocation)) + "/" + qwkpacket->bbsqwkid + ".REP";
        if (createRepPacket(fileName)) {
            QDir sp(QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkpacket->bbsqwkid + "/"));

            sp.removeRecursively();
        }
    }
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionAbout_triggered()
{
    About ab(this);

    ab.exec();
}

void MainWindow::on_actionSettings_triggered()
{
    Settings sw(this);

    sw.exec();
}

void MainWindow::on_MainWindow_destroyed()
{

}

void MainWindow::on_areaList_cellClicked(int row, int column)
{

}

void MainWindow::doupdate() {
    ui->msgText->setFontFamily(editor_font);
    ui->msgText->setFontPointSize(editor_font_size);
    QString text = ui->msgText->toPlainText();
    ui->msgText->clear();
    ui->msgText->setPlainText(text);
}



void MainWindow::on_msgTable_itemSelectionChanged()
{
    if (ui->msgTable->selectedItems().length() > 0) {
        int row = ui->msgTable->selectedItems().first()->row();
        if (qwkpacket != nullptr && selected_group != -1) {
            for (int i = 0; i < qwkpacket->groups->at(selected_group)->msgs->count(); i++) {
                if (ui->msgTable->item(row, 0)->text().toInt() == qwkpacket->groups->at(selected_group)->msgs->at(i)->msgno) {
                   selected_msg = i;
                   bool read = qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->read;

                   if (!read) {
                       int areano;
                       for (areano=0;areano < ui->areaList->rowCount();areano++) {
                           if (qwkpacket->groups->at(selected_group)->groupname == ui->areaList->item(areano, 0)->text()) {
                               qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->read = true;
                               qwkpacket->groups->at(selected_group)->unread--;
                               ui->areaList->item(areano, 2)->setText(tr("%1").arg(qwkpacket->groups->at(selected_group)->unread));
                               ui->areaList->item(areano, 3)->setText(tr("%1").arg(qwkpacket->groups->at(selected_group)->msgs->count()));
                               ui->areaList->item(areano, 1)->setText(tr("%1").arg(qwkpacket->groups->at(selected_group)->personal));
                               break;
                           }
                       }


                       QFont font(ui->msgTable->font());

                       font.setBold(false);

                       qwkpacket->update_read(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->offset, true);


                       ui->msgTable->item(selected_msg, 0)->setFont(font);
                       ui->msgTable->item(selected_msg, 1)->setFont(font);
                       ui->msgTable->item(selected_msg, 2)->setFont(font);
                       ui->msgTable->item(selected_msg, 3)->setFont(font);
                       ui->msgTable->item(selected_msg, 4)->setFont(font);

                   }
                   ui->msgText->setStyleSheet(tr("background-color:black;color:white;font-family:%1;font-size:%2pt;").arg(editor_font).arg(editor_font_size));
                   if (qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->ansibody != "") {
                       ui->msgText->setHtml(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->ansibody);
                   } else {
                       ui->msgText->setText(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->body);
                   }

                   ui->fromLabel->setText(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->from);
                   ui->toLabel->setText(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->to);
                   ui->subjLabel->setText(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->subject);
                   ui->dateLabel->setText(qwkpacket->groups->at(selected_group)->msgs->at(selected_msg)->date.toString());
                   return;
                }
            }
        }
    }
}

void MainWindow::on_areaList_itemSelectionChanged()
{
    if (ui->areaList->selectedItems().length() > 0) {
        int row = ui->areaList->selectedItems().first()->row();
        if (qwkpacket != nullptr) {
            int areano = 0;

            for (areano=0;areano < qwkpacket->groups->count();areano++) {
                if (qwkpacket->groups->at(areano)->groupname == ui->areaList->item(row, 0)->text()) {
                    msgViewState = ui->msgTable->horizontalHeader()->saveState();
                    this->selected_group = areano;
                    ui->msgText->setText("");
                    ui->fromLabel->setText("");
                    ui->toLabel->setText("");
                    ui->subjLabel->setText("");
                    ui->dateLabel->setText("");
                    ui->msgTable->clearContents();
                    ui->msgTable->setColumnCount(5);
                    ui->msgTable->setRowCount(qwkpacket->groups->at(areano)->msgs->count());
                    ui->msgTable->setSortingEnabled(false);
                    for (int j=0;j<qwkpacket->groups->at(areano)->msgs->count(); j++) {
                        int read = qwkpacket->groups->at(areano)->msgs->at(j)->read;

                        QFont font(ui->msgTable->font());
                        QColor color(Qt::GlobalColor::black);
                        if (read) {
                            font.setBold(false);
                        } else {
                            font.setBold(true);
                        }

                        if (qwkpacket->groups->at(areano)->msgs->at(j)->personal) {
                            color = Qt::GlobalColor::blue;
                        }

                        QTableWidgetItem *msgno = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(areano)->msgs->at(j)->msgno));
                        msgno->setFont(font);
                        msgno->setForeground(color);
                        ui->msgTable->setItem(j, 0, msgno);
                        QTableWidgetItem *subj = new QTableWidgetItem(qwkpacket->groups->at(areano)->msgs->at(j)->subject);
                        subj->setFont(font);
                        subj->setForeground(color);
                        ui->msgTable->setItem(j, 1, subj);
                        QTableWidgetItem *from = new QTableWidgetItem(qwkpacket->groups->at(areano)->msgs->at(j)->from);
                        from->setFont(font);
                        from->setForeground(color);
                        ui->msgTable->setItem(j, 2, from);
                        QTableWidgetItem *to = new QTableWidgetItem(qwkpacket->groups->at(areano)->msgs->at(j)->to);
                        to->setFont(font);
                        to->setForeground(color);
                        ui->msgTable->setItem(j, 3, to);
                        QTableWidgetItem *date = new QTableWidgetItem(qwkpacket->groups->at(areano)->msgs->at(j)->date.toString());
                        date->setFont(font);
                        date->setForeground(color);
                        ui->msgTable->setItem(j, 4, date);
                    }
                    ui->msgTable->setSortingEnabled(false);
                    ui->msgTable->horizontalHeader()->restoreState(msgViewState);
                    return;
                }
            }
        }
    }
}

void MainWindow::refresh_area_list() {
    int count = 0;
    if (qwkpacket != nullptr) {
        ui->areaList->clear();
        if (hide_empty_areas) {
            for (int i = 0; i < qwkpacket->groups->count(); i++) {
                if (qwkpacket->groups->at(i)->msgs->count() > 0) {
                    count++;
                }
            }
        } else {
            for (int i = 0; i < qwkpacket->groups->count(); i++) {
                count++;
            }
        }

        ui->areaList->setRowCount(count);
        count = 0;
        ui->areaList->setHorizontalHeaderItem(0, new QTableWidgetItem("Group name"));
        ui->areaList->setHorizontalHeaderItem(1, new QTableWidgetItem(QIcon(":/mail-personal.svg"), "Personal"));
        ui->areaList->setHorizontalHeaderItem(2, new QTableWidgetItem(QIcon(":/mail-new.svg"), "Unread"));
        ui->areaList->setHorizontalHeaderItem(3, new QTableWidgetItem(QIcon(":/mail.svg"), "Total"));
        ui->areaList->setColumnCount(4);

        if (hide_empty_areas) {
            for (int i = 0; i < qwkpacket->groups->count(); i++) {
                if (qwkpacket->groups->at(i)->msgs->count() > 0) {
                    QTableWidgetItem *AreaName = new QTableWidgetItem(qwkpacket->groups->at(i)->groupname);
                    ui->areaList->setItem(count, 0, AreaName);
                    QTableWidgetItem *MsgUnread = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(i)->unread));
                    QTableWidgetItem *MsgTotal = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(i)->msgs->count()));
                    QTableWidgetItem *MsgPersonal = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(i)->personal));

                    MsgUnread->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                    MsgTotal->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                    MsgPersonal->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

                    ui->areaList->setItem(count, 2, MsgUnread);
                    ui->areaList->setItem(count, 3, MsgTotal);
                    ui->areaList->setItem(count, 1, MsgPersonal);
                    count++;
                }
            }
        } else {
            for (int i = 0; i < qwkpacket->groups->count(); i++) {
                QTableWidgetItem *AreaName = new QTableWidgetItem(qwkpacket->groups->at(i)->groupname);
                ui->areaList->setItem(i, 0, AreaName);
                QTableWidgetItem *MsgUnread = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(i)->unread));
                QTableWidgetItem *MsgTotal = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(i)->msgs->count()));
                QTableWidgetItem *MsgPersonal = new QTableWidgetItem(tr("%1").arg(qwkpacket->groups->at(i)->personal));

                MsgUnread->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                MsgTotal->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                MsgPersonal->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

                ui->areaList->setItem(i, 2, MsgUnread);
                ui->areaList->setItem(i, 3, MsgTotal);
                ui->areaList->setItem(i, 1, MsgPersonal);

            }
        }
    }
}

void MainWindow::on_actionHide_Empty_Areas_toggled(bool arg1)
{
    hide_empty_areas = arg1;
     QSettings settings("Andrew Pamment", "TitanMail");
     settings.setValue("hide_empty_areas", hide_empty_areas);
    refresh_area_list();
}


void MainWindow::on_actionFetch_QWK_via_FTP_triggered()
{
    FetchDialog fd(this);
    fd.exec();

}

void MainWindow::ftpUploadFinished(bool success, QString reason) {
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (dir.exists()) {
        QFile file(dir.path() + "/temp.rep");
        if (file.exists()) {
            file.remove();
        }
    }
    if (success) {
        QDir sp(QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkpacket->bbsqwkid + "/"));
        sp.removeRecursively();
    } else {
        QMessageBox mb(QMessageBox::Icon::Critical, "FTP Error Occured", reason, QMessageBox::StandardButton::Ok);
        mb.exec();
    }
    setEnabled(true);
}

void MainWindow::ftpDownloadFinished(bool success, QString reason) {
    if (success) {
        QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
        openQwkFile(dir.path() + "/temp.qwk");
    } else {
        QMessageBox mb(QMessageBox::Icon::Critical, "FTP Error Occured", reason, QMessageBox::StandardButton::Ok);
        mb.exec();
    }
    setEnabled(true);
}
/*
void MainWindow::ftpRequestFinished(QNetworkReply *reply) {
    if (reply->url().path().endsWith(".QWK", Qt::CaseInsensitive)) {
        if (reply->error() == QNetworkReply::NoError) {
            QByteArray data = reply->readAll();
            QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
            if (!dir.exists()) {
                QDir().mkpath(dir.path());
            }
            QSaveFile file(dir.path() + "/temp.qwk");
            file.open(QIODevice::WriteOnly);
            file.write(data);
            file.commit();

            openQwkFile(dir.path() + "/temp.qwk");
        } else {
            QMessageBox mb(QMessageBox::Icon::Critical, "FTP Error Occured", reply->errorString(), QMessageBox::StandardButton::Ok);
            mb.exec();
        }
    } else if (reply->url().path().endsWith(".REP", Qt::CaseInsensitive)) {

        if (reply->error() == QNetworkReply::NoError) {
            QDir sp(QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + qwkpacket->bbsqwkid + "/"));
            sp.removeRecursively();
        }
    }
    setEnabled(true);
}
*/
void MainWindow::on_actionPut_REP_via_FTP_triggered()
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!dir.exists()) {
        QDir().mkpath(dir.path());
    }

    if (createRepPacket(dir.path() + "/temp.rep")) {
        FetchDialog fd(this, false);
        fd.exec();
    }
}


void MainWindow::on_actionAbout_QT_triggered()
{
    QMessageBox::aboutQt(this);
}


void MainWindow::on_actionView_Outbound_triggered()
{
    if (qwkpacket != nullptr) {
        Outbound ob(this);
        if (ob.load()) {
            ob.exec();
        } else {
            QMessageBox mb(QMessageBox::Icon::Information, "No outbound messages", "No outbound messages to view!", QMessageBox::StandardButton::Ok);
            mb.exec();
        }
    } else {
        QMessageBox mb(QMessageBox::Icon::Information, "No QWK Packet", "Please open a qwk packet first!", QMessageBox::StandardButton::Ok);
        mb.exec();
    }
}

