#include <QStandardPaths>
#include <QDir>
#include <QTemporaryFile>
#include "editoutbound.h"
#include "ui_editoutbound.h"
#include "mainwindow.h"
#include "outbound.h"

EditOutbound::EditOutbound(QWidget *parent, MainWindow *mw, struct ob_msg *obmsg) :
    QDialog(parent),
    ui(new Ui::EditOutbound)
{
    ui->setupUi(this);
    ob = (Outbound *)parent;
    this->mw = mw;
    msg = obmsg;

    ui->subjectEdit->setText(msg->subj);
    ui->toEdit->setText(msg->to);

    ui->messageEdit->setPlainText(msg->body);
}

EditOutbound::~EditOutbound()
{
    delete ui;
}

void EditOutbound::on_buttonBox_accepted()
{
   msg->to = ui->toEdit->text();
   msg->subj = ui->subjectEdit->text();
   msg->body = ui->messageEdit->toPlainText();
   // delete old message
   QDir().remove(msg->filename);
   // save message

   QString SavePath = QString(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + mw->qwkpacket->bbsqwkid + "/" + "msg.XXXXXX");
   QDir p;
   p.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + mw->qwkpacket->bbsqwkid + "/");
   QTemporaryFile file(SavePath);
   char buffer[64];
   if (file.open()) {
      file.setAutoRemove(false);
      QString filename = file.fileName(); // stop removing my file!!
      QString qwkestr = "";

      memset(msg->hdr.MsgTo, ' ', 25);
      memcpy(msg->hdr.MsgTo, ui->toEdit->text().toStdString().c_str(), (ui->toEdit->text().length() < 25 ? ui->toEdit->text().length() : 25));

      if (mw->qwkesupport && ui->toEdit->text().length() > 25) {
          qwkestr += "To: " + ui->toEdit->text() + "\n";
      }

      memset(msg->hdr.MsgFrom, ' ', 25);
      memcpy(msg->hdr.MsgFrom, msg->from.toStdString().c_str(), (msg->from.length() < 25 ? msg->from.length() : 25));

      if (mw->qwkesupport && msg->from.length() > 25) {
          qwkestr += "From: " + msg->from + "\n";
      }

      memset(msg->hdr.MsgSubj, ' ', 25);
      memcpy(msg->hdr.MsgSubj, ui->subjectEdit->text().toStdString().c_str(), (ui->subjectEdit->text().length() < 25 ? ui->subjectEdit->text().length() : 25));

      if (mw->qwkesupport && ui->subjectEdit->text().length() > 25) {
           qwkestr += "Subject: " + ui->subjectEdit->text() + "\n";
      }


      QString msgbody = "";

       if (mw->qwkesupport && qwkestr.length() > 0) {
           msgbody += qwkestr + "\n";
       }

       msgbody += ui->messageEdit->toPlainText();

       int msgrecs = msgbody.length() / 128;
       if (msgrecs * 128 < msgbody.length()) {
           msgrecs += 1;
       }

       int msgbytes = msgrecs * 128;

       char *msgbodybytes = (char *)malloc(msgbytes);

       memset(msgbodybytes, ' ', msgbytes);
       for (int i=0;i<msgbody.length();i++) {
           if (msgbody.at(i) == '\n') {
               msgbodybytes[i] = '\xe3';
           } else {
               msgbodybytes[i] = QwkPacket::convert_cp437(msgbody.toStdString()).at(i);
           }
       }
       memset(buffer, '\0', 64);
       snprintf(buffer, 7, "%d", msgrecs + 1);
       memset(msg->hdr.Msgrecs, ' ', 7);
       memcpy(msg->hdr.Msgrecs, buffer, strlen(buffer));
       file.write((const char *)&msg->hdr, sizeof(struct QwkHeader));
       file.write(msgbodybytes, msgbytes);

       free(msgbodybytes);
       file.close();
   }

   // reload outbound
   ob->load();
   close();
}

