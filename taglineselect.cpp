#include <fstream>
#include <QRandomGenerator>
#include "newmsg.h"
#include "taglineselect.h"
#include "ui_taglineselect.h"

TaglineSelect::TaglineSelect(QWidget *parent, QString tagline_file) :
    QDialog(parent),
    ui(new Ui::TaglineSelect)
{
    ui->setupUi(this);
    nmw = (NewMsg *)parent;
    std::vector<std::string> taglines;
    std::ifstream file(tagline_file.toStdString());
    std::string str;
    while (std::getline(file, str))
    {
        ui->listWidget->addItem(QString::fromStdString(str));
    }
    file.close();

    srand(time(NULL));

    if (ui->listWidget->count() > 0) {
        int r = QRandomGenerator::global()->bounded(ui->listWidget->count());
        ui->listWidget->item(r)->setSelected(true);
        ui->listWidget->scrollToItem(ui->listWidget->selectedItems().at(0));
    }
}

TaglineSelect::~TaglineSelect()
{
    delete ui;
}

void TaglineSelect::on_buttonBox_accepted()
{
    if (ui->listWidget->selectedItems().count() > 0) {
        nmw->addMessage(ui->listWidget->selectedItems().at(0)->text());
    } else {
        nmw->addMessage("");
    }
    close();
    deleteLater();
}


void TaglineSelect::on_buttonBox_rejected()
{
    nmw->addMessage("");
    close();
    deleteLater();
}

