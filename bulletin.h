#ifndef BULLETIN_H
#define BULLETIN_H

#include <QDialog>

namespace Ui {
class Bulletin;
}

class Bulletin : public QDialog
{
    Q_OBJECT

public:
    explicit Bulletin(QWidget *parent = nullptr);
    ~Bulletin();

    bool load(QString file);
private:
    Ui::Bulletin *ui;
};

#endif // BULLETIN_H
