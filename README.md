#### About

Titan Mail is an offline QWK mail reader used with bulletin board systems, like [Synchronet](https://synchro.net/).

It has built in ZIP support thanks to [QuaZip](https://github.com/stachenov/quazip), although an alternative archiver can be specified.

It also has built in FTP support if your bulletin board system supports sending / receiving qwk packets via FTP.

#### Compiling

Titan Mail is known to compile on Linux and FreeBSD (as well as Windows)

Make sure you have the following prerequisits installed

 * QT5 or QT6
 * development headers
 * libcurl
 * ZLib & ZLib development headers
 * CMake
 * A C++ compiler like gcc or clang

Clone the repository:

	`git clone --recurse-submodules https://gitlab.com/apamment/TitanMail`

Create a build directory:

	`mkdir TitanMail-build`

Run CMAKE:

	`cd TitanMail-build && cmake ../TitanMail`

Run Make:

	`make`

Install with:

	`sudo make install`


By default it will install with the prefix /usr/local, so you may need to ensure the library folders are in your search path.
