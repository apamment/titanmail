#ifndef MESSAGE_H
#define MESSAGE_H

#include <QString>
#include <QDateTime>

class Message
{
public:
    Message(int no, QString subject, QString from, QString to, QDateTime date, QString body, QString ansibody);

    QString subject;
    QString from;
    QString to;
    QString body;
    QString ansibody;
    QDateTime date;
    uint32_t offset;
    bool personal;
    bool read;
    int msgno;
};

#endif // MESSAGE_H
